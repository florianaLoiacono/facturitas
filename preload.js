const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('facturita', {
  getInvoices: async (templatePath, csvPath, namePattern) => ipcRenderer.invoke('facturita:generate-invoices', templatePath, csvPath, namePattern),
  removeValue: () => 0,
  getValue: (name) => ipcRenderer.invoke("facturita:get-value",name),
  persistValue: async (name, value) => ipcRenderer.invoke("facturita:persist-value", name, value)
});

