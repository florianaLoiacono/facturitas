const { app, BrowserWindow, ipcMain, nativeTheme } = require('electron')
const path = require('path')
const facturita = require('./facturita');

const fs = require('fs/promises');
const DATA_FOLDER = "data"
const DATA_PATH = `./${DATA_FOLDER}`;


function createWindow() {
  const win = new BrowserWindow({
    width: 900,
    height: 700,
    backgroundColor: '#fff',
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })
  win.loadFile('index.html');
}

ipcMain.handle('facturita:save-template-path', async (ev, name, path) => {
  fs.readFile(templatePath, null, (err, data) => {
    if (err) { console.log(err); return false; }
    fs.writeFile(`${DATA_PATH}/template-file.docx`, data, (err) => {
      if (err) return false;
      return true;
    })
  });
});

ipcMain.handle('facturita:persist-value', async (ev, name, value) => {
  let data;
  try {

    data = await fs.readFile(`./${DATA_PATH}/values`, { encoding: 'utf8' });

    let obj = data ? JSON.parse(data) : {};
    obj[name] = value;
    await fs.writeFile(`${DATA_PATH}/values`, JSON.stringify(obj));
    return true;
  } catch (err) {
    console.error(err);
    if (err.code !== 'ENOENT')
      return false;
  }
});

ipcMain.handle('facturita:get-value', async (ev, name) => {
  let data;
  try {
    data = await fs.readFile(`${DATA_PATH}/values`, { encoding: 'utf8' });
  } catch (err) {
    console.error(err);
    return undefined;
  }

  let obj = JSON.parse(data);
  return obj[name];
});

ipcMain.handle('facturita:get-csv-header', async (ev, path) => {

  fs.readFile(`${path}`, null, (err, data) => {
    if (err) { console.log(err); return false; }
    let lines = data.split('/n');
    return lines[0];
  });
});

ipcMain.handle('facturita:generate-invoices', async (ev, templatePath, csvPath, namePattern) => {
  facturita.getInvoices(templatePath, csvPath, namePattern);
});


app.whenReady().then(() => {
  createWindow();

  fs.readdir(`.`, (err, folders) => {
    if (folders.findIndex(x => x.match(DATA_FOLDER)) == -1)
      fs.mkdir(`${DATA_PATH}`, (err, path) => { });
  });

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

