const fs = require('fs/promises');
const DATA_PATH = "./data";
const docx = require('docx');
const FOLDER_RESULT_NAME = 'result-facturitas';
module.exports = {
    getInvoices: async (templatePath, csvPath, namePattern = '', selectedPath = '.') => {
        try {
            console.log(process.cwd(), "getInvoices init");

            await checkResultFolder(selectedPath);

            let rows = [];
            rows = await convertCsvToObject(csvPath);

            persistArrProps(`${selectedPath}/${FOLDER_RESULT_NAME}`, rows);
            
            if (!rows) return;

            rows.forEach(e => {
                e.option = { patches: {} };
                for (const [key, value] of Object.entries(e)) {
                    let fieldName = key.toLowerCase();
                    e.option.patches[fieldName] = {
                        type: docx.PatchType.PARAGRAPH,
                        children: [new docx.TextRun(value.toString())],
                    }
                }
            });

            if (rows.length === 0) return;

            let template = await fs.readFile(templatePath);
            let keyNames = getPatternPositions(namePattern, Object.keys(rows[1]));

            console.log(namePattern, 'namePattern');
            for (let ele of rows) {
                let doc = await docx.patchDocument(template, ele.option);
                let fileName = composeFileName(namePattern, keyNames, ele);
                await fs.writeFile(`${selectedPath}/${FOLDER_RESULT_NAME}/${fileName}.docx`, doc);
            }
            return true;

        } catch (ex) {
            console.error(ex, `ERROR`)
            return false;
        }
    }
}

async function checkResultFolder(selectPath) {
    // fs.lstat(`${selectPath}/${FOLDER_RESULT_NAME}`);
    let folders = await fs.readdir(`${selectPath}`);
    if (folders.findIndex(x => x.match(FOLDER_RESULT_NAME)) == -1)
        fs.mkdir(`${selectPath}/${FOLDER_RESULT_NAME}`);
    return;
}

function getPatternPositions(pattern, objKeys) {
    if (pattern === '') {
        return Date.now();
    } else {

        let notFinished = true;
        let positions = [];

        let contPatt = pattern;
        while (notFinished) {
            let startStrIndx = contPatt.indexOf('{{');

            if (startStrIndx == -1) { notFinished = false; break; }

            const endStrIndex = contPatt.indexOf('}}');
            let position = Number.parseInt(contPatt.slice(startStrIndx + 2, endStrIndex)) - 1;

            let value = contPatt.slice(startStrIndx + 2, endStrIndex);
            positions.push({ value, key: objKeys[position] });
            contPatt = contPatt.slice(endStrIndex + 2);
        }

        return positions;
    }
}
function composeFileName(pattern, possitions, obj) {
    possitions.forEach(x => {
        pattern = pattern.replace(`{{${x.value}}}`, obj[x.key]);
    })
    return pattern;
}

function validateParamType(val, type) {
    console.log(typeof val == type, 'typeof val == type');
    console.log(typeof val, 'typeof val');
    console.log(type, 'type');
    if (typeof val == type)
        return;
    else throw 'Bad parameter';
}

async function convertCsvToObject(csvPath) {
    let data = await fs.readFile(csvPath, { encoding: 'utf8' });

    // Check windows
    let dataRows = data.split('\r\n');
    if (dataRows == 1)
        dataRows = data.toString().split('\n');

    // get headers
    let propsNames = dataRows[0].split(';').map(x =>
        x.toLowerCase().replace(' ', '_').replace(/[^\x00-\x7F]/g, "")
    );

    let values = [];
    for (let i = 1; i < dataRows.length; i++) {
        if (dataRows[i].length == 0) break;

        let rowSplited = csvRowToObject(propsNames, dataRows[i].split(';'));
        if (rowSplited != undefined)
            values.push(rowSplited);
    }
    console.log(values.length, 'cantidad de valores');

    return values;
}

const csvRowToObject = (propsNames, content) => {
    let res = {};
    let countEmpty = 0;
    content.forEach((e, i) => {
        if (e.length == 0)
            countEmpty++;

        res[propsNames[i]] = e;
    })
    if (countEmpty == content.length)
        return undefined;
    else
        return res;
}

const persistArrProps = async (path, values) => {
    await fs.writeFile(`${path}/${Date.now()}.json`, JSON.stringify(values));
}