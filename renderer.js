const DEFAULT_FILENAME_PATTERN = "{{1}}. Factura {{4}} - {{2}}_{{3}}";
const CLICK = "click";
const CHANGE = "change";
const KEYUP = "keyup";
const setInitEventListeners = () => {
    addInitEventListenerById("template", CHANGE, handleTemplate);
    addInitEventListenerById("csv", CHANGE, handleCSV);
    addInitEventListenerById("namePattern", CHANGE, enablePersistPatternButton);
    addInitEventListenerById("namePattern", KEYUP, enablePersistPatternButton);
    addInitEventListenerById("getInvoices", CLICK, clickGenerateInvoices);
    addInitEventListenerById("persistNamePattern", CLICK, clickPersistPattern)
}

const addInitEventListenerById = (id, actionName, fn, option = false, props = []) => {
    const element = document.getElementById(id);
    element.addEventListener(actionName, fn, option);

}

let tmpFile = undefined;
let csvFile = undefined;

const handleTemplate = async (ev) => {
    // Template
    let file = ev.target.files[0];

    // let res = await window.facturita.setTemplatePath(file);
    tmpFile = file;

    // draw information
    let removeButton = document.createElement('button');
    removeButton.id = 'remove-template';
    removeButton.classList.add('remove');
    removeButton.append('X');
    removeButton.addEventListener('click', async () => {
        tmpFile = undefined;
        // await window.facturita.removeTemplate();
        document.getElementById('template').value = null;
        let div = document.getElementById('template-path-selected');
        div.innerHTML = '';

        submitBtnVisibility();
    });

    let div_button = document.createElement('div');
    div_button.classList.add("actions");
    div_button.appendChild(removeButton);

    let div_name = document.createElement('div');
    div_name.innerText = file.name;
    div_name.classList.add("name");

    let div_selected = document.getElementById('template-path-selected');
    div_selected.innerHTML = '';

    div_selected.appendChild(div_button);
    div_selected.appendChild(div_name);

    submitBtnVisibility();
}

async function handleCSV(ev) {
    // CSV
    let file = ev.target.files[0];
    // let res = await window.facturita.setCSVPath(file);
    csvFile = file;

    // draw information
    let removeButton = document.createElement('button');
    removeButton.id = 'remove-csv';
    removeButton.classList.add('remove');
    removeButton.append('X');
    removeButton.addEventListener('click', async () => {
        csvFile = undefined;
        document.getElementById('csv').value = null;
        let div = document.getElementById('csv-path-selected');
        div.innerHTML = '';

        document.getElementById('csv-fields').innerHTML = '';
        submitBtnVisibility();
    });

    let div_button = document.createElement('div');
    div_button.classList.add("actions");
    div_button.appendChild(removeButton);

    let div_name = document.createElement('div');
    div_name.innerText = file.name;
    div_name.classList.add("name");

    let div_selected = document.getElementById('csv-path-selected');
    div_selected.innerHTML = '';

    div_selected.appendChild(div_button);
    div_selected.appendChild(div_name);

    submitBtnVisibility();

    getCSVHeaders(csvFile);
}

async function clickGenerateInvoices() {
    let namePattern = document.getElementById('namePattern').value;
    await window.facturita.getInvoices(tmpFile.path, csvFile.path, namePattern);
}

async function getCSVHeaders(text) {
    var reader = new FileReader();
    reader.readAsText(csvFile, "UTF-8");
    reader.onload = function (evt) {
        let a = evt.target.result.split('\r\n');
        if (a.length == 1)
            a = evt.target.result.split('\n');

        let divRowName = document.createElement('div');
        divRowName.classList.add('row');
        let divRowPos = document.createElement('div');
        divRowPos.classList.add('row');

        a[0].split(';').forEach((x, i) => {
            let cellName = document.createElement('div');
            cellName.classList.add('cell');
            cellName.innerHTML = x;
            divRowName.appendChild(cellName)

            let cellPos = document.createElement('div');
            cellPos.classList.add('cell')
            cellPos.innerHTML = i + 1;

            divRowPos.appendChild(cellPos);

        });
        let tableDiv = document.getElementById('csv-fields');
        tableDiv.appendChild(divRowName);
        tableDiv.appendChild(divRowPos);
    }
    reader.onerror = function (evt) {
        console.log("error reading file");
    }
}

async function enablePersistPatternButton(ev) {
    let value = '';
    if (ev)
        value = ev.target.value;
    else
        value = document.getElementById('namePattern').value;

    let patterns = [DEFAULT_FILENAME_PATTERN, await window.facturita.getValue('filename-pattern')]

    if (!patterns.includes(value)) {
        document.querySelector('.submit.block').querySelector('#persistNamePattern').disabled = false;
    } else {
        document.querySelector('.submit.block').querySelector('#persistNamePattern').disabled = true;
    }
}

function submitBtnVisibility() {
    if (tmpFile != undefined && csvFile != undefined) {
        document.querySelector('.submit.block').querySelector('#namePattern').disabled = false;
    } else {
        document.querySelector('.submit.block').querySelector('#namePattern').disabled = true;
    }
}

async function clickPersistPattern() {
    let value = document.getElementById('namePattern').value;
    await window.facturita.persistValue('filename-pattern', value);
    enablePersistPatternButton();
}

async function checkIfSavedPattern() {
    debugger;
     let patternInput = document.getElementById('namePattern');
    let savedPattern = await window.facturita.getValue('filename-pattern');
    console.log(savedPattern)
    if (!savedPattern)
        patternInput.value = DEFAULT_FILENAME_PATTERN;
    else
        patternInput.value = savedPattern;
    document.querySelector('.submit.block').querySelector('#persistNamePattern').disabled = true;

}

setInitEventListeners();
submitBtnVisibility();
checkIfSavedPattern();



let now = Date.now();
let fooVar = window.localStorage.getItem('foo');
if (!window.localStorage.getItem('foo')) {
    console.log(now);
    window.localStorage.setItem('foo', now);
} else {
    console.log(fooVar);
}